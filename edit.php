<?php
if(isset($_POST['update_student']))
{
    $student_id = mysqli_real_escape_string($con, $_POST['student_id']);

    $nip = mysqli_real_escape_string($con, $_POST['nip']);
    $nodokumen = mysqli_real_escape_string($con, $_POST['noDokumen']);
    $namadokumen = mysqli_real_escape_string($con, $_POST['namaDokumen']);
    $status = mysqli_real_escape_string($con, $_POST['status']);
    $catatan = mysqli_real_escape_string($con, $_POST['catatan']);

    if($nip == NULL || $nodokumen == NULL || $namadokumen == NULL)
    {
        $res = [
            'status' => 422,
            'message' => 'All fields are mandatory'
        ];
        echo json_encode($res);
        return;
    }

    $query = "UPDATE tabel_dok SET nip='$nip', no_dokumen='$nodokumen', nama_dokumen='$namadokumen', status='$status', catatan='$catatan' 
                WHERE id='$student_id'";
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
        $res = [
            'status' => 200,
            'message' => 'Student Updated Successfully'
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 500,
            'message' => 'Student Not Updated'
        ];
        echo json_encode($res);
        return;
    }
}


if(isset($_GET['student_id']))
{
    $student_id = mysqli_real_escape_string($con, $_GET['student_id']);

    $query = "SELECT * FROM tabel_dok WHERE id='$student_id'";
    $query_run = mysqli_query($con, $query);

    if(mysqli_num_rows($query_run) == 1)
    {
        $student = mysqli_fetch_array($query_run);

        $res = [
            'status' => 200,
            'message' => 'Student Fetch Successfully by id',
            'data' => $student
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 404,
            'message' => 'Student Id Not Found'
        ];
        echo json_encode($res);
        return;
    }
}
?>