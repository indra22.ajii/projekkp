<?php

require 'dbcon.php';

if(isset($_POST['save_dokumen']))
{
    $nip = mysqli_real_escape_string($con, $_POST['nip']);
    $nodokumen = mysqli_real_escape_string($con, $_POST['noDokumen']);
    $status = mysqli_real_escape_string($con, $_POST['status']);
    $catatan = mysqli_real_escape_string($con, $_POST['catatan']);
    

    if($nip == NULL || $nodokumen == NULL )
    {
        $res = [
            'status' => 422,
            'message' => 'All fields are mandatory'
        ];
        echo json_encode($res);
        return;
    }

    $target_directory = "upload/";
    $filename= $_FILES["file"]["name"];
    $target_file = $target_directory.basename($_FILES["file"]["name"]);   //name is to get the file name of   uploaded file
    $filetype = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $newfilename = $target_directory.$_FILES["file"]["name"];
    
    if(move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename));
    else echo 0;

    $query = "INSERT INTO tabel_dok (nip,no_dokumen,nama_dokumen,status,catatan) VALUES ('$nip','$nodokumen','$filename','$status','$catatan')";
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
        $res = [
            'status' => 200,
            'message' => 'Dokumen Berhasil Dibuat'
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 500,
            'message' => 'Dokumen Tidak Berhasil Dibuat'
        ];
        echo json_encode($res);
        return;
    }
}


if(isset($_POST['update_student']))
{
    $student_id = mysqli_real_escape_string($con, $_POST['student_id']);

    $nip = mysqli_real_escape_string($con, $_POST['nip']);
    $nodokumen = mysqli_real_escape_string($con, $_POST['noDokumen']);
    $status = mysqli_real_escape_string($con, $_POST['status']);
    $catatan = mysqli_real_escape_string($con, $_POST['catatan']);

    if($nip == NULL || $nodokumen == NULL || $status == NULL || $catatan == NULL)
    {
        $res = [
            'status' => 422,
            'message' => 'All fields are mandatory'
        ];
        echo json_encode($res);
        return;
    }
    $query_hapusfile = "SELECT nama_dokumen from tabel_dok WHERE id='$student_id'";
    $query_runhapusfile = mysqli_query($con, $query_hapusfile);
    $hasil = mysqli_fetch_assoc($query_runhapusfile);

    $target_directory = "upload/";
    $filename= $_FILES["file"]["name"];
    $target_file = $target_directory.basename($_FILES["file"]["name"]);   //name is to get the file name of   uploaded file
    $filetype = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $newfilename = $target_directory.$_FILES["file"]["name"];
    
    if(move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename));
    else echo 0;

    $query = "UPDATE tabel_dok SET nip='$nip', no_dokumen='$nodokumen', nama_dokumen='$filename', status='$status', catatan='$catatan' 
                WHERE id='$student_id'";
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
        unlink("./upload/$hasil[nama_dokumen]");
        $res = [
            'status' => 200,
            'message' => 'Student Updated Successfully'
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 500,
            'message' => 'Student Not Updated'
        ];
        echo json_encode($res);
        return;
    }
}


if(isset($_GET['student_id']))
{
    $student_id = mysqli_real_escape_string($con, $_GET['student_id']);

    $query = "SELECT * FROM tabel_dok WHERE id='$student_id'";
    $query_run = mysqli_query($con, $query);

    if(mysqli_num_rows($query_run) == 1)
    {
        $student = mysqli_fetch_array($query_run);

        $res = [
            'status' => 200,
            'message' => 'Student Fetch Successfully by id',
            'data' => $student
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 404,
            'message' => 'Student Id Not Found'
        ];
        echo json_encode($res);
        return;
    }
}

if(isset($_POST['delete_student']))
{
    $student_id = mysqli_real_escape_string($con, $_POST['student_id']);
    $query_hapusfile = "SELECT nama_dokumen from tabel_dok WHERE id='$student_id'";
    $query_runhapusfile = mysqli_query($con, $query_hapusfile);
    $hasil = mysqli_fetch_assoc($query_runhapusfile);
    $query = "DELETE FROM tabel_dok WHERE id='$student_id'";
    $query_run = mysqli_query($con, $query);
    
    if($query_run)
    {
        unlink("./upload/$hasil[nama_dokumen]");
        $res = [
            'status' => 200,
            'message' => 'Dokumen Berhasil Dihapus'
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 500,
            'message' => 'Dokumen Tidak Berhasil Dihapus'
        ];
        echo json_encode($res);
        return;
    }
}
