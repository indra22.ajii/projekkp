<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link href="style.css" rel="stylesheet"> -->
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">

    <title>PHP CRUD using jquery ajax without page reload</title>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
</head>

<body>

    <!-- Tambah Dokumen -->
    <div class="modal fade" id="dokumenAddModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Dokumen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="saveDokumen">
                    <div class="modal-body">

                        <div id="errorMessage" class="alert alert-warning d-none"></div>

                        <div class="mb-3">
                            <label for="">NIP</label>
                            <input type="text" name="nip" class="form-control" />
                        </div>
                        <div class="mb-3">
                            <label for="">No Dokumen</label>
                            <input type="text" name="noDokumen" class="form-control" />
                        </div>
                        <div class="mb-3">
                            <label for="">Nama Dokumen</label>
                            <!-- <input type="text" name="namaDokumen" class="form-control" /> -->
                            <input type="file" id="fileUpload" class="form-control" />
                        </div>
                        <div class="mb-3">
                            <label for="">Satus</label>
                            <select class="form-select" name="status" aria-label="Default select example">
                                <option selected disabled>Buka Untuk Mengubah Satus</option>
                                <option value="Diproses">Diproses</option>
                                <option value="Diterima">Diterima</option>
                                <option value="Ditolak">Ditolak</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="">Catatan</label>
                            <input type="text" name="catatan" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Student Modal -->
    <div class="modal fade" id="studentEditModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="updateStudent">
                    <div class="modal-body">

                        <div id="errorMessageUpdate" class="alert alert-warning d-none"></div>

                        <input type="hidden" name="student_id" id="student_id">

                        <div class="mb-3">
                            <label for="">NIP</label>
                            <input type="text" id="updateNip" name="nip" class="form-control" />
                        </div>
                        <div class="mb-3">
                            <label for="">No Dokumen</label>
                            <input type="text" id="updateNodok" name="noDokumen" class="form-control" />
                        </div>
                        <div class="mb-3">
                            <label for="">Nama Dokumen</label>
                            <!-- <input type="text" name="namaDokumen" class="form-control" /> -->
                            <input type="file" id="fileUpdate" class="form-control" />
                        </div>
                        <div class="mb-3">
                            <label for="">Satus</label>
                            <select class="form-select" id="updateStatus" name="status" aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <option value="Diproses">Diproses</option>
                                <option value="Diterima">Diterima</option>
                                <option value="Ditolak">Ditolak</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="">Catatan</label>
                            <input type="text" id="updateCatatan" name="catatan" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update Student</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- View Student Modal -->
    <div class="modal fade" id="studentViewModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">View Student</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="mb-3">
                        <label for="">NIP</label>
                        <p id="nip" class="form-control"></p>
                    </div>
                    <div class="mb-3">
                        <label for="">No Dokumen</label>
                        <p id="no_dok" class="form-control"></p>
                    </div>
                    <div class="mb-3">
                        <label for="">Nama Dokumen</label>
                        <p id="name_dok" class="form-control"></p>
                    </div>
                    <div class="mb-3">
                        <label for="">Status</label>
                        <p id="status" class="form-control"></p>
                    </div>
                    <div class="mb-3">
                        <label for="">Catatan</label>
                        <p id="catatan" class="form-control"></p>
                    </div>
                    <div class="mb-3">
                        <label for="">Tanggal Masuk</label>
                        <p id="tanggal" class="form-control"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Tampilkan Data Table -->
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border:none">
                    <div class="card-header">
                        <h4>BERKAS KENAIKAN PEGAWAI

                            <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#dokumenAddModal">
                                Tambah Dokumen
                            </button>
                        </h4>
                    </div>
                    <div class="card-body">

                        <div id="showtable"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#showtable').load("table.php")
        });
    </script>
    <script>
        $(document).on('submit', '#saveDokumen', function(e) {
            e.preventDefault();

            var formData = new FormData(this);
            var file_data = $('#fileUpload').prop('files')[0];
            formData.append("save_dokumen", true);
            formData.append("file", file_data);


            $.ajax({
                type: "POST",
                url: "code.php",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {

                    console.log(response)
                    var res = jQuery.parseJSON(response);
                    if (res.status == 422) {
                        $('#errorMessage').removeClass('d-none');
                        $('#errorMessage').text(res.message);

                    } else if (res.status == 200) {

                        $('#errorMessage').addClass('d-none');
                        $('#dokumenAddModal').modal('hide');
                        $('#saveDokumen')[0].reset();

                        alertify.set('notifier', 'position', 'top-right');
                        alertify.success(res.message);

                        $('#showtable').load("table.php")

                    } else if (res.status == 500) {
                        alert(res.message);
                    }
                }
            });
        });

        $(document).on('click', '.editStudentBtn', function() {

            var student_id = $(this).val();

            $.ajax({
                type: "GET",
                url: "code.php?student_id=" + student_id,
                success: function(response) {

                    var res = jQuery.parseJSON(response);
                    if (res.status == 404) {

                        alert(res.message);
                    } else if (res.status == 200) {
                        $('#student_id').val(res.data.id);
                        $('#updateNip').val(res.data.nip);
                        $('#updateNodok').val(res.data.no_dokumen);
                        $('#updateNamadok').val(res.data.nama_dokumen);
                        $('#updateStatus').val(res.data.status);
                        $('#updateCatatan').val(res.data.catatan);

                        $('#studentEditModal').modal('show');
                    }

                }
            });

        });

        $(document).on('submit', '#updateStudent', function(e) {
            e.preventDefault();

            
            var formData = new FormData(this);
            var file_data = $('#fileUpdate').prop('files')[0];
            formData.append("update_student", true);
            formData.append("file", file_data);

            $.ajax({
                type: "POST",
                url: "code.php",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {

                    var res = jQuery.parseJSON(response);
                    if (res.status == 422) {
                        $('#errorMessageUpdate').removeClass('d-none');
                        $('#errorMessageUpdate').text(res.message);

                    } else if (res.status == 200) {

                        $('#errorMessageUpdate').addClass('d-none');

                        alertify.set('notifier', 'position', 'top-right');
                        alertify.success(res.message);

                        $('#studentEditModal').modal('hide');
                        $('#updateStudent')[0].reset();

                        $('#showtable').load("table.php")

                    } else if (res.status == 500) {
                        alert(res.message);
                    }
                }
            });

        });

        $(document).on('click', '.viewStudentBtn', function() {

            var student_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "code.php?student_id=" + student_id,
                success: function(response) {

                    var res = jQuery.parseJSON(response);

                    if (res.status == 404) {

                        alert(res.message);
                    } else if (res.status == 200) {

                        $('#nip').text(res.data.nip);
                        $('#no_dok').text(res.data.no_dokumen);
                        $('#name_dok').text(res.data.nama_dokumen);
                        $('#status').text(res.data.status);
                        $('#catatan').text(res.data.catatan);
                        $('#tanggal').text(res.data.tgl_masuk);

                        $('#studentViewModal').modal('show');
                    }
                }
            });
        });

        $(document).on('click', '.deleteStudentBtn', function(e) {
            e.preventDefault();

            if (confirm('Are you sure you want to delete this data?')) {
                var student_id = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "code.php",
                    data: {
                        'delete_student': true,
                        'student_id': student_id
                    },
                    success: function(response) {

                        var res = jQuery.parseJSON(response);
                        if (res.status == 500) {

                            alert(res.message);
                        } else {
                            alertify.set('notifier', 'position', 'top-left');
                            alertify.success(res.message);

                            $('#showtable').load("table.php")
                        }
                    }
                });
            }
        });
    </script>

</body>

</html>