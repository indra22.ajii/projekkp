<?php

require 'dbcon.php';

if(isset($_POST['save_dokumen']))
{
    $nip = mysqli_real_escape_string($con, $_POST['nip']);
    $nodokumen = mysqli_real_escape_string($con, $_POST['noDokumen']);
    // $namadokumen = mysqli_real_escape_string($con, $_POST['namaDokumen']);
    $status = mysqli_real_escape_string($con, $_POST['status']);
    $catatan = mysqli_real_escape_string($con, $_POST['catatan']);
    

    if($nip == NULL || $nodokumen == NULL )
    {
        $res = [
            'status' => 422,
            'message' => 'All fields are mandatory'
        ];
        echo json_encode($res);
        return;
    }

    $target_directory = "upload/";
    $filename= $_FILES["file"]["name"];
    $target_file = $target_directory.basename($_FILES["file"]["name"]);   //name is to get the file name of   uploaded file
    $filetype = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $newfilename = $target_directory.$_FILES["file"]["name"];
    
    //move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename);   // tmp_name is the file temprory stored     in the server
    
    //Now to check if uploaded or not
    
    if(move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename));
    else echo 0;

    $query = "INSERT INTO tabel_dok (nip,no_dokumen,nama_dokumen,status,catatan) VALUES ('$nip','$nodokumen','$filename','$status','$catatan')";
    $query_run = mysqli_query($con, $query);

    if($query_run)
    {
        $res = [
            'status' => 200,
            'message' => 'Dokumen Berhasil Dibuat'
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 500,
            'message' => 'Dokumen Tidak Berhasil Dibuat'
        ];
        echo json_encode($res);
        return;
    }
}
?>