 <table id="datatables" class="table table-bordered">
     <thead>
         <tr>
             <th style="text-align:center">No</th>
             <th>NIP</th>
             <th style="text-align:center">No Dokumen</th>
             <th style="text-align:center">Nama Dokumen</th>
             <th>Status</th>
             <th>Catatan</th>
             <th>Tanggal Masuk</th>
             <th>Action</th>
         </tr>
     </thead>
     <tbody>
         <?php
            require 'dbcon.php';

            $query = "SELECT * FROM tabel_dok";
            $query_run = mysqli_query($con, $query);
            $no = 1;

            if (mysqli_num_rows($query_run) > 0) {
                foreach ($query_run as $student) {
            ?>
                 <tr>
                     <td><?= $no++ ?></td>
                     <td><?= $student['nip'] ?></td>
                     <td><?= $student['no_dokumen'] ?></td>
                     <td><a href="./upload/<?= $student['nama_dokumen'] ?>" target="_blank"> Klik Disini</a></td>
                     <td><?= $student['status'] ?></td>
                     <td><?= $student['catatan'] ?></td>
                     <td><?= $student['tgl_masuk'] ?></td>
                     <td>
                         <button type="button" value="<?= $student['id']; ?>" class="viewStudentBtn btn btn-info btn-sm">View</button>
                         <button type="button" value="<?= $student['id']; ?>" class="editStudentBtn btn btn-success btn-sm">Edit</button>
                         <button type="button" value="<?= $student['id']; ?>" class="deleteStudentBtn btn btn-danger btn-sm">Delete</button>
                     </td>
                 </tr>
         <?php
                }
            }
            ?>

     </tbody>
 </table>

 <script>
     $(document).ready(function() {
         $('#datatables').DataTable();
     })
 </script>