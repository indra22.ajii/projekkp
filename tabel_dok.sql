-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2022 at 01:15 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_dok`
--

CREATE TABLE `tabel_dok` (
  `id` int(11) NOT NULL,
  `nip` bigint(20) NOT NULL,
  `no_dokumen` varchar(50) NOT NULL,
  `nama_dokumen` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `catatan` text NOT NULL,
  `tgl_masuk` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_dok`
--

INSERT INTO `tabel_dok` (`id`, `nip`, `no_dokumen`, `nama_dokumen`, `status`, `catatan`, `tgl_masuk`) VALUES
(150, 5645646546748, '4535435', 'LaporanPraktikum5_18524057_EkaPutraP.pdf', 'Diproses', 'sada', '2022-08-08 03:26:17'),
(151, 746545, 'nojhgfgh876', 'Surat-Pengumuman-Pendanaan-PKM-Skema-Pendanaan-Tahun-2022-Diktiristek.pdf', 'Diproses', 'gijhgjh', '2022-08-07 13:16:38'),
(152, 216534762, 'nkasdjk123', 'novie_lucky,+Journal+manager,+4_Upadewa_V3N1_Lucya (1).pdf', 'Diproses', 'AGGFJADSH', '2022-08-08 00:30:32'),
(153, 12537, 'ashgdhas', 'admin,+Fuad+Bachtiar_Kehidupan+sosial+NAPZA+(PROOFED)+(2).pdf', 'Diproses', 'wdajsdhg', '2022-08-08 01:42:56'),
(154, 2364, 'we7', '15126-Article Text-41629-1-10-20211018.pdf', 'Diterima', 'asdhasf', '2022-08-08 01:43:22'),
(155, 9, 'aweghfdas', 'Surat-Pengumuman-Pendanaan-PKM-Skema-Pendanaan-Tahun-2022-Diktiristek.pdf', 'Ditolak', 'wegaghsd', '2022-08-08 01:43:56'),
(156, 23421, 'ashdfjg', 'LaporanPraktikum5_18524057_EkaPutraP.pdf', 'Diterima', 'awhejfjadsf', '2022-08-08 01:44:32'),
(163, 5213, 'hjsjgfa', 'PROPOSAL_Potensi_Sektor_Pariwisata_dan_P.docx', 'Ditolak', 'afsdgfg', '2022-08-09 06:35:13'),
(164, 1165236, 'agjsgdh', 'PROPOSAL_Potensi_Sektor_Pariwisata_dan_P.docx', 'Ditolak', 'hagsdjgh', '2022-08-09 06:35:49'),
(165, 65172356, 'asdgjasgdg', 'PROPOSAL_Potensi_Sektor_Pariwisata_dan_P.docx', 'Diterima', 'asghdjaghs', '2022-08-09 06:36:11'),
(166, 21583768, 'gasjhgdj', 'PROPOSAL_Potensi_Sektor_Pariwisata_dan_P.docx', 'Diproses', 'sahgdjag', '2022-08-09 06:36:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_dok`
--
ALTER TABLE `tabel_dok`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_dok`
--
ALTER TABLE `tabel_dok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
